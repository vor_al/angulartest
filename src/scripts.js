var app = angular.module("twoTable", []);

app.factory('httpWithPreLoader', ['$http', '$rootScope', function($http, $rootScope) {
    $http.defaults.transformRequest.push(function(data) {
        $rootScope.$broadcast('httpStarted');
        return data;
    });
    $http.defaults.transformResponse.push(function(data) {
        $rootScope.$broadcast('httpStopped');
        return data;
    });
    return $http;
}]);

app.directive('preLoader', function(){
    return {
        scope: {
            showLoader: '='
        },
        template: '<div class="pre-loader" ng-show="showLoader"><img src="/pics/loader.gif" alt="" /></div>'
    }
});

app.directive('myElement', function() {
    return {
        restrict: 'EA',
        replace: false,
        scope: false,
        template:
            '<td>{{ item.id }}</td>' +
            '<td>{{ item.title }}</td>' +
            '<td><img ng-attr-src="{{ item.image }}" width="100" alt=""/></td>' +
            '<td ng-show="checkboxes"><input type="checkbox" ng-model="item.checked" /></td>',
        compile: function() {
            return {
                'pre': function(scope, element, attributes) {
                    scope.checkboxes = attributes.checkboxes == "1" ? 1 : 0;
                }
            }
        }
    }
});

app.controller('TwoTableController', ['$scope', 'httpWithPreLoader',
    function($scope, $httpWithPreLoader) {

        $scope.is_preloader = false;
        $scope.movedItems = [];
        $scope.isSavedAllTable2Items = true;

        $scope.$on('httpStarted', function() {
            $scope.is_preloader = true;
        });

        $scope.$on('httpStopped', function() {
            $scope.is_preloader = false;
        });


        $scope.hasChecked = function() {
            var count = 0;
            angular.forEach($scope.rowItems, function(item) {
                count +=  item.checked ? 1 : 0;
            });
            return count;
        };

        $scope.loadData = function() {
            $httpWithPreLoader.post('/getData.php').success(function(response) {
                $scope.rowItems = response.data;
                $scope.movedItems = response.table2Data;
            });
        };

        $scope.deleteData = function() {
            if ($scope.hasChecked()) {
                var oldItems = $scope.rowItems;
                $scope.rowItems = [];
                angular.forEach(oldItems, function (item) {
                    if (!item.checked) {
                        $scope.rowItems.push(item);
                    }
                });
            }
        };

        $scope.moveData = function() {
            if ($scope.hasChecked()) {
                angular.forEach($scope.rowItems, function (item) {
                    if (item.checked) {
                        $scope.movedItems.push(item);
                    }
                });
                $scope.isSavedAllTable2Items = false;
                $scope.deleteData();
            }
        };

        $scope.saveTable2Data = function() {
            $httpWithPreLoader.post('/saveTable2Data.php', $scope.movedItems).success(function() {
                $scope.isSavedAllTable2Items = true;
            });
        };

    }]
);
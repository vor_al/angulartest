describe('app', function() {
    var scope, controller;
    beforeEach(function() {
        module('twoTable');
    });

    describe('testing TwoTableController', function () {
        beforeEach(inject(function($rootScope, $controller) {
            scope = $rootScope.$new();
            controller = $controller('TwoTableController', {'$scope': scope});
            scope.rowItems = [
                {
                    id: 1,
                    title: 'title1',
                    image: '/pics/3dwall01.jpg',
                    checked: false
                }, {
                    id: 2,
                    title: 'title2',
                    image: '/pics/3dwall02.jpg',
                    checked: true
                }, {
                    id: 3,
                    title: 'title3',
                    image: '/pics/3dwall03.jpg',
                    checked: false
                }
            ];
            scope.movedItems = [];
        }));

        it('should delete checked rows', function () {
            scope.deleteData();
            expect(scope.rowItems.length).toEqual(2);
            expect(scope.rowItems).toEqual([
                {
                    id: 1,
                    title: 'title1',
                    image: '/pics/3dwall01.jpg',
                    checked: false
                }, {
                    id: 3,
                    title: 'title3',
                    image: '/pics/3dwall03.jpg',
                    checked: false
                }
            ]);
            expect(scope.movedItems).toEqual([]);
        });

        it('should move checked rows to another array', function () {
            scope.moveData();
            expect(scope.rowItems.length).toEqual(2);
            expect(scope.movedItems.length).toEqual(1);
            expect(scope.rowItems).toEqual([
                {
                    id: 1,
                    title: 'title1',
                    image: '/pics/3dwall01.jpg',
                    checked: false
                }, {
                    id: 3,
                    title: 'title3',
                    image: '/pics/3dwall03.jpg',
                    checked: false
                }
            ]);
            expect(scope.movedItems).toEqual([{
                id: 2,
                title: 'title2',
                image: '/pics/3dwall02.jpg',
                checked: true
            }]);
        });
    });

});


<?php
session_start();

$count = 25;

$ids = range(1, $count);
$str = 'You might wonder how the remote service knows the name of the function to wrap the returned JSON in.
    The answer is, that you pass that name to the remote service along with the other request parameters.
    The function name is one of the request parameters. By default this parameter name is callback but you will have
    to check with the concrete service to see what parameter name it expects for the function name. ';
$names = explode(' ', $str);

$data = array();
$table2Data = array();

foreach ($ids as $id) {
    $name = '';
    while (strlen($name) < 4) {
        $name = array_shift($names);
        if (count($names) == 0) break;
    }
    $item = array(
        'id' => $id,
        'title' => $name,
        'image' => '/pics/3dwall' . sprintf('%02d', $id) . '.jpg',
        'checked' => false
    );
    if (!isset($_SESSION['table2Data'][$item['id']])) {
        $data []= $item;
    } else {
        $table2Data []= $item;
    }
}

for ($i = 0; $i < 50000000; $i++) {
    ;;;;
}


echo json_encode(array('data' => $data, 'table2Data' => $table2Data));